import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class mainForm extends JFrame {

    private JPanel panel1;
    private JButton btnConvert;
    private JTextField tfSellerID;
    private static JFrame parent;

    private static Properties prop;

    private static Long startTime;

    private static String sellerID;

    public mainForm()
    {
        parent= new JFrame();
        add(panel1);

        setTitle("Converter CAW OLEOD-DCC V1.0");
        setSize(800,350);

        getConfigFile();

        tfSellerID.setText(prop.getProperty("app.sellerID"));

        btnConvert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnConvert.setEnabled(false);
                startTime= null;
                startTime = System.nanoTime();
                sellerID= tfSellerID.getText();
                if (sellerID.equals("")){
                    JOptionPane.showMessageDialog(parent, "Instruction At (set in configuration file) has been passed");
                }else{
                    convert();
                }
                btnConvert.setEnabled(true);
            }
        });
    }

    private void convert() {
        File dir= new File(prop.getProperty("app.folder_in"));
        String[] fileList= dir.list();

        if (fileList.length==0){
            JOptionPane.showMessageDialog(parent, "File conversion error!! Please check the input file.");
        }

        int recordCount;

        for (String nameFile: fileList){
            recordCount=0;
            try {
                FileReader fileReader = new FileReader(prop.getProperty("app.folder_in")+nameFile);
                CSVReader csvReader = new CSVReader(fileReader);
                List<String[]> records = csvReader.readAll();

                String[] out= nameFile.split("\\.");
                File file = new File(prop.getProperty("app.folder_out")+out[0]+"_converted.csv");
                //menghasilkan output di folder OUT sesuai nama folder IN
                FileWriter outputfile = new FileWriter(file);
                CSVWriter writer = new CSVWriter(outputfile, ',', '\u0000', '\u0000', "\n");

                for (String[] record:records){
                    recordCount+=1;

                    SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
                    //Parsing the given String to Date object
                    Date tempDate = formatter.parse(record[3]);
                    String invoiceDate= new SimpleDateFormat("YYYYMMdd").format(tempDate);

                    String dueDate=record[5];
                    if(dueDate.equals("")){
                        dueDate=record[6];
                    }
                    tempDate= formatter.parse(dueDate);
                    dueDate= new SimpleDateFormat("YYYYMMdd").format(tempDate);

                    tempDate= formatter.parse(record[4]);
                    String invoiceExpireDate= new SimpleDateFormat("YYYYMMdd").format(tempDate);

                    writer.writeNext(new String[]{"D", sellerID, record[0], "N", record[1], record[2], invoiceDate, invoiceExpireDate,
                            dueDate, record[7], record[8], "P", "0", "IDR", "0", record[7], record[8], record[9], "N",
                            record[11], record[10], record[12], record[13], "B"});
                }

                long endTime   = System.nanoTime();
                long totalTime = endTime - startTime;
                float sec= totalTime/100000000;
                int seconds= (int)sec;
                JOptionPane.showMessageDialog(parent, "Successfully converting "+recordCount+" record(s) \n in "+seconds+" seconds");

                writer.close();
            } catch (IOException | CsvException | ParseException e) {
                JOptionPane.showMessageDialog(parent, "File conversion error!! Please check the input file.");
                e.printStackTrace();
            }
        }
    }

    private void getConfigFile() {
        prop= new Properties();
        String properties= "app.config";

        InputStream config = null;
        try {
            config = new FileInputStream(properties);
            prop.load(config);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
